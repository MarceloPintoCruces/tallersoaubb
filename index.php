<?php
	
	/**
	Ejemplo API RESTful Básica
	*/

	require_once('DataBaseHandler.php'); 

	//Un manejador de la BD. Deben bajar adodb desde http://adodb.org/dokuwiki/doku.php y manejar las dependencias en esta clase (DataBaseHandler)

	//Autentificación HTTP - Descomentar si quieren usarla.

	/*if(!isset($_SERVER['PHP_AUTH_USER']) || !isset($_SERVER['PHP_AUTH_PW'])) {
	    header("WWW-Authenticate: Basic realm=\"Pagina protegida\"");
	    header("HTTP\ 1.0 401 Unauthorized");
	    echo 'No estas autorizado para ver este contenido.';
	    exit;
	}*/

	$method = $_SERVER['REQUEST_METHOD'];

	//Dependiendo el método de la petición, 
	switch ($method) {
		case 'GET':
			getMethod();
			break;

		case 'POST':
			postMethod();
			break;			

		case 'PUT':
			putMethod();
			break;	

		case 'DELETE':
			deleteMethod();
			break;	

		default:
			break;
	}

	function getMethod()
	{
		header("HTTP/1.1 200 OK" );
		header('Content-Type: application/json');
		$response['status']=200;
		$response['status_message']="OK";
		$response['data']="";

		$i = 0;

		$db = DataBaseHandler::getInstance()->connect();

		$query = "SELECT * FROM contacto";

		$data = $db->Execute($query);

		while($data && !$data->EOF)
		{
			$contactos[$i]['nombre'] = $data->fields[1];
			$contactos[$i]['apellido'] = $data->fields[2];
			$contactos[$i]['celular'] = $data->fields[3];
			$contactos[$i]['email'] = $data->fields[4];
			$i++;
			$data->MoveNext();
		}

		//Codificamos a formato JSON
		echo json_encode(array('contactos'=>$contactos));
	}

	function postMethod()
	{
		header("HTTP/1.1 200 OK" );
		header('Content-Type: application/json');
		$response['status']=200;
		$response['status_message']="OK";
		$response['data']="";
		
		$db = DataBaseHandler::getInstance()->connect();

		$data = json_decode(file_get_contents('php://input'));

		$datos['nombre'] = $data->nombre;
		$datos['apellido'] = $data->apellido;
		$datos['celular'] = $data->celular;
		$datos['email'] = $data->email;

		if ($db->AutoExecute('contacto',$datos, 'INSERT')) {
			echo json_encode(array('exito'=>true));
		}else{
			echo json_encode(array('exito'=>false));
		}
		

		
	}


?>