<?php

function getPrecio($servicio)
{
	# consulta sql puede ir acá. Simulamos retornando un valor cualquiera.

	if ($servicio == "esterilizacion") {
		return 30000;
	}elseif ($servicio == "control") {
		return 5000;
	}

	return 0;

}
require('nusoap/lib/nusoap.php'); //Ubicación de nusoap - https://sourceforge.net/projects/nusoap/

    $server = new soap_server();

    $server->configureWSDL('servicioserver', 'urn:precio');

    $server->register("getPrecio",
                    array('servicio' => 'xsd:string'),
                    array('return' => 'xsd:decimal'),
                    'urn:precio',
                    'urn:precio#getPrecio');

    $HTTP_RAW_POST_DATA = isset($HTTP_RAW_POST_DATA)
                          ? $HTTP_RAW_POST_DATA : '';
    $server->service($HTTP_RAW_POST_DATA);


?>