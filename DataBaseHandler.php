<?php

	require_once('adodb5/adodb.inc.php'); //Manejar el directorio de ADO DB.
	require_once('datos.db.php'); //Acá los datos de conexión. (Acá solo se manejan como constantes, cambiar a gusto)
	
	/**
	* Class: DataBaseHandler
	* Implementación de Singleton
	*/

	class DataBaseHandler 
	{

		private $database;
		private $host;
		private $user;
		private $password;
		private $db;
		static private $instance;
		
		private function __construct()
		{
			$this->database = database;
			$this->host = host;
			$this->user = user;
			$this->password = password;
		}

		static public function getInstance()
		{
			if(self::$instance == null){
				self::$instance = new DataBaseHandler();
			}
			return self::$instance;
		}

		public function connect()
		{

			if($this->db == null){
				$this->db=NewADOConnection('mysql'); //Usar driver de mysql
				$this->db->Connect($this->host,$this->user,$this->password,$this->database);
				return $this->db;
			}else{
				return $this->db;
			}			
		}


	}

?>